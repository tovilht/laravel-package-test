<?php

namespace Tovi\Testing;

use App\Http\Controllers\Controller;


class DevelopmentController extends Controller
{
    public function healthCheck()
    {
        return response()->json([
            'message' => 'OK from package in Admin by Woo'
        ], 200);
    }
}
