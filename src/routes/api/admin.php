<?php

// Default Imports

use Illuminate\Support\Facades\Route;
// Controllers

Route::group(
    ['prefix' => '/tests'],
    function () {
        Route::get('/package', function () {
            return 'package healthy';
        });
    }
);
